# dictionary

An App Vesto project for intership.




1. FlutterDictionary class:

- class initialization;

- evoking dictionary from the code;

- detailed overview of dictionary functions and variables;

2. Locale Saving:

- saving locale;

- locale usage;

3. RTL & LTR:

- RTL-languages;

- features of RTL-languages;

4. Initialization of FlutterDictionary:

- localizationDelegates;

- supportedLocales;





## Getting Started

For every text in the project that is visible on the user interface we must have a placeholder in the dictionary file . Then on the actual page - the
text value will come from that file!
