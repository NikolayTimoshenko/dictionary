import 'package:dictionary/dictionary/dictionary_classes/general_language.dart';
import 'package:dictionary/dictionary/dictionary_classes/home_page_language.dart';
import 'package:flutter/material.dart';

class Language {
  final GeneralLanguage generalLanguage;
  final HomePageLanguage homePageLanguage;

  const Language({
    @required this.generalLanguage,
    @required this.homePageLanguage,
  });
}
