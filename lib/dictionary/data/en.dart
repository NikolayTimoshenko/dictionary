import 'package:dictionary/dictionary/dictionary_classes/general_language.dart';
import 'package:dictionary/dictionary/dictionary_classes/home_page_language.dart';
import 'package:dictionary/dictionary/models/language.dart';

const Language en = Language(
  generalLanguage: GeneralLanguage(
    appTitle: 'Test App',
  ),
  homePageLanguage: HomePageLanguage(
    fresh: 'Fresh!',
    titleGame: 'Game',
    titleShop: 'Shop',
    toContactUs: 'To contact us:',
    buyNow: 'Buy now',
    contact: 'Administrator: +38 (068) 42-93-776',
    banner1: 'Huge selection of games!',
    banner2: 'Best game of the Year!',
    banner3: 'Easy money making!',
    description1: 'Game of the Year',
    description2: 'Old Legends',
    description3: 'Revolution in the world of games',
    description4: 'Become a real samurai',
    description5: 'Unknown novelty',
    description6: 'Become a king!',
    money: '\$',
  ),
);
