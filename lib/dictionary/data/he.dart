import 'package:dictionary/dictionary/dictionary_classes/general_language.dart';
import 'package:dictionary/dictionary/dictionary_classes/home_page_language.dart';
import 'package:dictionary/dictionary/models/language.dart';

const Language he = Language(
  generalLanguage: GeneralLanguage(
    appTitle: 'בדיקת אפליקציה',
  ),
  homePageLanguage: HomePageLanguage(
    fresh: 'טָרִי!',
    titleGame: 'מִשְׂחָק',
    titleShop: 'לִקְנוֹת',
    toContactUs: 'ליצור קשר איתנו:',
    buyNow: 'קנה עכשיו',
    contact: 'מנהל מערכת : ' + '+38 (068) 42-93-776',
    banner1: 'מבחר ענק של משחקים!',
    banner2: 'המשחק הטוב ביותר של השנה!',
    banner3: 'להרוויח כסף קל!',
    description1: 'משחק השנה',
    description2: 'אגדות ישנות',
    description3: 'מהפכה בעולם המשחקים',
    description4: 'הפוך לסמוראי אמיתי',
    description5: 'חידוש לא ידוע',
    description6: 'להיות מלך!',
    money: '%',
  ),
);
