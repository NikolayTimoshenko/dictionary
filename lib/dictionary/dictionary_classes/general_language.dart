import 'package:flutter/material.dart';

class GeneralLanguage {
  final String appTitle;

  const GeneralLanguage({
    @required this.appTitle,
  });

  factory GeneralLanguage.fromJson(Map<String, dynamic> json) {
    return GeneralLanguage(
      appTitle: json['appTitle'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'appTitle': appTitle,
    };
  }
}
