import 'package:dictionary/res/style/app_colors.dart';
import 'package:dictionary/res/style/app_shadows.dart';
import 'package:flutter/material.dart';

class AppFonts {
  static TextStyle get titleGameHomePage {
    return TextStyle(
      color: AppColors.purple,
      fontSize: 30.0,
      shadows: AppShadows.textShadowGame,
    );
  }

  static TextStyle get titleShopHomePage {
    return TextStyle(
      color: AppColors.orange,
      fontSize: 30.0,
      shadows: AppShadows.textShadowShop,
    );
  }

  static TextStyle get bannerItemTitle {
    return TextStyle(
      fontSize: 33.0,
      color: Colors.white,
      shadows: AppShadows.textShadowTitle,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get gridViewItemSubtitle {
    return TextStyle(
      fontSize: 12.0,
    );
  }

  static TextStyle get toContactUsTextHomePage {
    return TextStyle(
      fontSize: 25.0,
      color: AppColors.purple,
    );
  }

  static TextStyle get contactUsTextHomePage {
    return TextStyle(
      fontSize: 20.0,
      color: AppColors.orange,
    );
  }

  static TextStyle get langListLocale {
    return TextStyle(
      fontSize: 20.0,
    );
  }
}
