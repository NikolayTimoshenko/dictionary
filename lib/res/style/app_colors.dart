import 'package:flutter/material.dart';

class AppColors {
  static final Color transparent = Color(0x00000000);
  static final Color blueAccent = Color(0xFF2979FF);
  static final Color yellow = Color(0xFFFFEB3B);
  static final Color black = Color(0xFF000000);
  static final Color orange = Color(0xFFFF9800);
  static final Color purple = Color(0xFF9C27B0);
  static final Color green = Color(0xFF4CAF50);
}
