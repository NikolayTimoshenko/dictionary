import 'package:dictionary/res/style/app_colors.dart';
import 'package:flutter/material.dart';

class AppShadows {
  static List<BoxShadow> textShadowGame = [
    BoxShadow(
      offset: Offset(-1.0, 2.0),
      blurRadius: 8.0,
      color: AppColors.blueAccent.withOpacity(0.4),
    ),
    BoxShadow(
      offset: Offset(1.0, 2.0),
      blurRadius: 8.0,
      color: AppColors.blueAccent.withOpacity(0.4),
    ),
  ];

  static List<BoxShadow> textShadowTitle = [
    BoxShadow(
      offset: Offset(-1.0, 2.0),
      blurRadius: 16.0,
      color: AppColors.black.withOpacity(0.9),
    ),
    BoxShadow(
      offset: Offset(1.0, 2.0),
      blurRadius: 16.0,
      color: AppColors.black.withOpacity(0.9),
    ),
  ];

  static List<BoxShadow> textShadowShop = [
    BoxShadow(
      offset: Offset(-1.0, 2.0),
      blurRadius: 8.0,
      color: AppColors.yellow.withOpacity(0.4),
    ),
    BoxShadow(
      offset: Offset(1.0, 2.0),
      blurRadius: 8.0,
      color: AppColors.yellow.withOpacity(0.4),
    ),
  ];
}
