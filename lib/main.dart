import 'package:dictionary/application/application.dart';
import 'package:dictionary/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      builder: (BuildContext ctx) {
        return LanguageProvider();
      },
      child: Application(),
    ),
  );
}
