import 'package:dictionary/dictionary/flutter_dictionary.dart';
import 'package:flutter/material.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLane(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}
