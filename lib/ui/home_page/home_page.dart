import 'package:auto_size_text/auto_size_text.dart';
import 'package:dictionary/dictionary/data/en.dart';
import 'package:dictionary/dictionary/dictionary_classes/home_page_language.dart';
import 'package:dictionary/dictionary/flutter_dictionary.dart';
import 'package:dictionary/res/app_fonts.dart';
import 'package:dictionary/res/style/app_colors.dart';
import 'package:dictionary/ui/widgets/banner_item.dart';
import 'package:dictionary/ui/widgets/drawer.dart';
import 'package:dictionary/ui/widgets/grid_view_item.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    HomePageLanguage language = FlutterDictionary.instance.language?.homePageLanguage ?? en.homePageLanguage;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: FlutterDictionary.instance.isRTL ? Alignment.centerLeft : Alignment.centerRight,
              width: MediaQuery.of(context).size.width / 3,
              child: AutoSizeText(
                '${language.titleGame} ',
                minFontSize: 20,
                style: AppFonts.titleGameHomePage,
              ),
            ),
            Container(
              alignment: FlutterDictionary.instance.isRTL ? Alignment.centerRight : Alignment.centerLeft,
              width: MediaQuery.of(context).size.width / 3,
              child: AutoSizeText(
                '${language.titleShop} ',
                minFontSize: 20,
                style: AppFonts.titleShopHomePage,
              ),
            ),
            Icon(
              Icons.games,
              size: 30.0,
            )
          ],
        ),
      ),
      drawer: MyDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Banner(
              message: language.fresh,
              color: AppColors.green,
              location: BannerLocation.topStart,
              child: Container(
                height: 200.0,
                width: double.infinity,
                decoration: BoxDecoration(
                  border: const Border.symmetric(
                    vertical: BorderSide(
                      width: 5.0,
                    ),
                  ),
                ),
                child: PageView(
                  children: <Widget>[
                    BannerItem(
                      path: 'https://app2top.ru/wp-content/uploads/2015/11/27-sovetov-po-sozdaniyu-horoshego-bannera-dlya-mobil-noj-igry-.png',
                      title: language.banner1,
                    ),
                    BannerItem(
                      path: 'https://app2top.ru/wp-content/uploads/2015/11/20.png',
                      title: language.banner2,
                    ),
                    BannerItem(
                      path: 'https://app2top.ru/wp-content/uploads/2015/11/Screenshot_15.png',
                      title: language.banner3,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            Container(
              height: (MediaQuery.of(context).size.width / 2) / 0.8 * 3,
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: GridView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  GridViewItem(
                    url: 'https://static.wikia.nocookie.net/587fac02-8aef-42de-9375-858f5c016480',
                    subTitle: language.description1,
                    price: 200,
                  ),
                  GridViewItem(
                    url: 'https://www.ferra.ru/images/426/426606.jpg',
                    subTitle: language.description2,
                    price: 80,
                  ),
                  GridViewItem(
                    url: 'https://trashbox.ru/ifiles/1262267_950698_screenshot_00.png-orig/subway-surfers-android-6.jpg',
                    subTitle: language.description3,
                    price: 220,
                  ),
                  GridViewItem(
                    url: 'https://vr36corp.ru/wp-content/uploads/2018/12/Fruit-Ninja-VR.jpg',
                    subTitle: language.description4,
                    price: 120,
                  ),
                  GridViewItem(
                    url: 'https://cs8.pikabu.ru/post_img/big/2017/12/17/4/1513488222197849793.png',
                    subTitle: language.description5,
                    price: 220,
                  ),
                  GridViewItem(
                    url: 'https://nexpro.ru/wp-content/uploads/2014/09/133.jpg',
                    subTitle: language.description6,
                    price: 120,
                  ),
                ],
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 0.8,
                  crossAxisSpacing: 10.0,
                  mainAxisSpacing: 10.0,
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            Container(
              child: Text(
                language.toContactUs,
                style: AppFonts.toContactUsTextHomePage,
                textAlign: TextAlign.start,
              ),
            ),
            Text(
              language.contact,
              style: AppFonts.contactUsTextHomePage,
            ),
            const SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }
}
