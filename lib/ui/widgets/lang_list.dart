import 'package:dictionary/dictionary/flutter_delegate.dart';
import 'package:dictionary/dictionary/models/supported_locales.dart';
import 'package:dictionary/providers/language_provider.dart';
import 'package:dictionary/res/app_const.dart';
import 'package:dictionary/res/app_fonts.dart';
import 'package:dictionary/res/style/app_colors.dart';
import 'package:dictionary/res/style/app_shadows.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LangList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250.0,
      margin: const EdgeInsets.all(10.0),
      child: Consumer<LanguageProvider>(
        builder: (BuildContext context, LanguageProvider provider, Widget child) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            for (Locale loc in SupportedLocales.instance.getSupportedLocales)
              InkWell(
                onTap: () => provider.setNewLane(loc),
                child: AnimatedContainer(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(
                      width: 5.0,
                      color: AppColors.black.withOpacity(0.5),
                    ),
                    boxShadow: FlutterDictionaryDelegate.getCurrentLocale.toString() == loc.toString() ? AppShadows.textShadowShop : null,
                    color: FlutterDictionaryDelegate.getCurrentLocale.toString() == loc.toString() ? AppColors.orange : AppColors.purple,
                  ),
                  padding: const EdgeInsets.all(15.0),
                  duration: Durations.milliseconds400,
                  child: Text(
                    loc.toString().toUpperCase(),
                    style: AppFonts.langListLocale,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
