import 'package:auto_size_text/auto_size_text.dart';
import 'package:dictionary/dictionary/flutter_dictionary.dart';
import 'package:dictionary/res/app_fonts.dart';
import 'package:flutter/material.dart';

class BannerItem extends StatelessWidget {
  final String path;
  final String title;

  const BannerItem({
    @required this.path,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Image.network(
              path,
              fit: BoxFit.fitWidth,
            ),
          ),
          Container(
            alignment: FlutterDictionary.instance.isRTL ? Alignment.bottomRight : Alignment.bottomLeft,
            padding: const EdgeInsets.all(16.0),
            child: AutoSizeText(
              title,
              maxLines: 2,
              textAlign: FlutterDictionary.instance.isRTL ? TextAlign.right : TextAlign.left,
              style: AppFonts.bannerItemTitle,
            ),
          ),
        ],
      ),
    );
  }
}
