import 'package:dictionary/dictionary/data/en.dart';
import 'package:dictionary/dictionary/flutter_dictionary.dart';
import 'package:dictionary/ui/widgets/lang_list.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              FlutterDictionary.instance.language?.generalLanguage?.appTitle ?? en.generalLanguage.appTitle,
            ),
          ),
          InkWell(
            child: LangList(),
          ),
        ],
      ),
    );
  }
}
