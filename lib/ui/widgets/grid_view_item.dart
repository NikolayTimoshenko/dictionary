import 'package:auto_size_text/auto_size_text.dart';
import 'package:dictionary/dictionary/data/en.dart';
import 'package:dictionary/dictionary/dictionary_classes/home_page_language.dart';
import 'package:dictionary/dictionary/flutter_dictionary.dart';
import 'package:dictionary/res/app_fonts.dart';
import 'package:dictionary/res/style/app_colors.dart';
import 'package:flutter/material.dart';

class GridViewItem extends StatelessWidget {
  final String url;
  final String subTitle;
  final int price;

  const GridViewItem({
    this.subTitle,
    this.price,
    this.url,
  });

  @override
  Widget build(BuildContext context) {
    HomePageLanguage language = FlutterDictionary.instance.language?.homePageLanguage ?? en.homePageLanguage;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(width: 4.0),
      ),
      child: GridTile(
        child: Image.network(
          url,
          fit: BoxFit.cover,
        ),
        footer: Container(
          height: 60.0,
          color: AppColors.black.withOpacity(0.8),
          child: ListTile(
            title: AutoSizeText(
              language?.buyNow ?? en.homePageLanguage.buyNow,
              maxLines: 1,
            ),
            subtitle: AutoSizeText(
              subTitle,
              overflow: TextOverflow.ellipsis,
              minFontSize: 12.0,
              maxLines: 2,
              style: AppFonts.gridViewItemSubtitle,
            ),
            trailing: Container(
              padding: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: Colors.green,
              ),
              child: Text(
                '$price.99 ${language.money}',
              ),
            ),
          ),
        ),
      ),
    );
  }
}
